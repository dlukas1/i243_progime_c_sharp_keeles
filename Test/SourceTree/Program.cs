﻿using System;

namespace SourceTree
{
    class MainClass
    {
		static void Main(string[] args)

		{

			//create the main array

			int[] myArray = new int[] { 1, 3, 5, 7, 9 };
			//The outside foreach loop to loop throught the array

			foreach (int intLoop in myArray)

			{

				//creates the spaces, takes the array number minus 1 then divide by 2

				//this gives you the amount of spaces needed for each level of the tree

				for (int iSpace = 0; iSpace < ((myArray[4] - intLoop) / 2); iSpace++)

				{

					System.Console.Write(" ");

				}

				//middle loop writes the asterisks "*" the full amount of current array[]

				for (int i = 0; i < intLoop; i++)

				{

					System.Console.Write("*");

				}

				//creates the spaces, takes the array number minus 1 then divide by 2

				//this gives you the amount of spaces needed for each level of the tree

				for (int iSpace = 0; iSpace < ((myArray[4] - intLoop) / 2); iSpace++)

				{

					System.Console.Write(" ");

				}

				//creates new lines after all 3 loops run

				System.Console.WriteLine("");

			}

			//nest this loop and do it 3 times

			for (int iBase = 0; iBase < myArray[1]; iBase++)

			{

				// now make the base of the tree

				for (int iSpaces = 0; iSpaces < myArray[1]; iSpaces++)

				{

					System.Console.Write(" ");

				}

				for (int iPipes = 0; iPipes < myArray[1]; iPipes++)

				{

					System.Console.Write("|");

				}

				// now make the base of the tree

				for (int iSpaces = 0; iSpaces < myArray[1]; iSpaces++)

				{

					System.Console.Write(" ");

				}

				//creates new lines after all 3 loops run

				System.Console.WriteLine("");

			}

		}

	}
}