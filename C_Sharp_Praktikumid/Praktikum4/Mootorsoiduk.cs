﻿using System;
namespace Praktikum4
{
    public class Mootorsoiduk
    {
        private int kiirus = 0;
        private int maxKiirus = 20;

        private Mootorsoiduk(int maxKiirus)
        {
            this.maxKiirus = maxKiirus;
        }
        private Mootorsoiduk() { }

        public virtual int Kiirenda()
        {
            kiirus = (kiirus + 10 <= maxKiirus) ? kiirus + 10 : maxKiirus;
            return kiirus;
        }

        private void Kiirenda(int x)
        {
            kiirus = (kiirus + x <= maxKiirus) ? kiirus + x : maxKiirus;
        }

        private void Stop() { kiirus = 0; }

        public virtual string HetkeSeis()
        {
            if (kiirus > 0)
            {
                return ("S6idab kiirusega " + kiirus);
            }
            else { return ("Seisab."); }
        }

        public override String ToString()
        { return HetkeSeis(); }



        public class Auto : Mootorsoiduk
        {

            public bool uksedLahti = false;

            public Auto()
                : base()
            {
                maxKiirus = 100;
            }

            public void AvaUksed()
            {
                kiirus = 0;
                uksedLahti = true;
            }

            public void SulgeUksed()
            {
                uksedLahti = false;
            }

            //Kiirenda(int kiirus) kirjutab üle baasklassi meetodi, 
            //kontrollib enne kiirendamist kas uksed on avatud.
            //Kui uksed on avatud, siis ei saa kiirendada.
            public override int Kiirenda()
            {
                if (uksedLahti == false)
                {
                    if (kiirus + 10 <= maxKiirus)
                    {
                        kiirus += 10;
                    }
                    else { kiirus = maxKiirus; }
                }
                else kiirus = 0;
                return kiirus;
            }

            public override string HetkeSeis()
            {
                return (uksedLahti == true) ? "Uksed lahti, seisab" : "S6idab kiirusega" + kiirus;
            }

            public override String ToString()
            { return HetkeSeis(); }

        }

        public class Veoauto : Auto
        {
            private bool Kallutab = false;
            public Veoauto() { maxKiirus = 70; }

            public void HakkaKallutama()
            {
                kiirus = 0;
                Kallutab = true;
            }
            public new void Kiirenda(int x)
            {
                if (Kallutab == true || uksedLahti == true)
                {
                    kiirus = 0;
                }
                else if (kiirus + x > maxKiirus) { kiirus = maxKiirus; }
                else { kiirus += x; }
            }

            public string Hetkeseis()
            {
                if (uksedLahti == true)
                {
                    return "Uksed lahti, seisab ";
                }
                else if (Kallutab == true) { return "Kallutab, seisab "; }
                else if (kiirus == 0) { return "Seisab"; }
                else { return "S6idab kiirusega " + kiirus; }
            }
            public new void ToString(){
                Console.WriteLine(Hetkeseis());
                Console.ReadLine();
            }
        }
    }
}

/*
Luua klass Veoauto, mis pärineb klassist Auto
Muutujad
Kallutab (vaikimisi false)
Konstruktor annab baasklassi konstruktori abil ette max kiiruseks 70
Meetodid:
HakkaKallutama() sõiduk peatatakse, hakkab kallutama
Kiirenda(int kiirus) juhul kui veoauto hetkel kallutab või uksed on avatud, siis ei saa kiirendada. Samuti ei tohi kiirus ületadaa maksimumkiirust.
Hetkeseis() tagastab tekstina auto hetkeseisu (kallutab, uksed avatud, sisab või sõidab kiirusega x)
ToString() panna väljastama Hetkeseis() poolt tagastatavat teksti
*/
