﻿NB! Kõik väljad on privaatsed 
Luua klass Mootorsoiduk

Muutujad
    Kiirus (vaikimisi 0)
    MaxKiirus (vaikimisi 20)

Konstruktor(int maxkiirus) – määratakse max kiirus

Meetodid
    Kiirenda() lisab kiirusele 10, aga kontrollib, et maksimumkiirust ei ületataks.
    Kiirenda(int kiirus) lisab kiirusele parameetri kiirus väärtuse ja kontrollib kas max kiirus on käes. Kui nii siis kiirus sellest suuremaks ei muutu.
    Stop() kiirus läheb nulli
    Hetkeseis() tagastab tekstina mootorsõiduki hetkeseisu (sõidab kiirusega x või seisab)
    ToString() panna väljastama Hetkeseis() poolt tagastatavat teksti

Auto pärineb klassist Mootorsoiduk
Muutujad
Uksed avatud (vaikimisi false)
Konstruktor annab baasklassi konstruktori abil ette max kiiruseks 100
Meetodid
AvaUksed() sõiduk peatatakse, uksed avatakse
SulgeUksed() sulgeb uksed
Kiirenda(int kiirus) kirjutab üle baasklassi meetodi, kontrollib enne kiirendamist kas uksed on avatud. Kui uksed on avatud, siis ei saa kiirendada.
Hetkeseis() tagastab tekstina auto hetkeseisu (uksed avatud, seisab või sõidab kiirusega x)
ToString() panna väljastama Hetkeseis() poolt tagastatavat teksti
Käsi_mittehoitud_tegevus

Luua klass Veoauto, mis pärineb klassist Auto
Muutujad
Kallutab (vaikimisi false)
Konstruktor annab baasklassi konstruktori abil ette max kiiruseks 70
Meetodid:
HakkaKallutama() sõiduk peatatakse, hakkab kallutama
Kiirenda(int kiirus) juhul kui veoauto hetkel kallutab või uksed on avatud, siis ei saa kiirendada. Samuti ei tohi kiirus ületadaa maksimumkiirust.
Hetkeseis() tagastab tekstina auto hetkeseisu (kallutab, uksed avatud, sisab või sõidab kiirusega x)
ToString() panna väljastama Hetkeseis() poolt tagastatavat teksti