﻿using System;

namespace Praktikum4
{
    class MainClass
    {
        public static void Main(string[] args)
        {


            Mootorsoiduk.Veoauto v = new Mootorsoiduk.Veoauto();
            v.ToString();
            v.Kiirenda(80);
            v.ToString();
            v.HakkaKallutama();
            v.ToString();



        }
    }
}
/*
 AvaUksed() sõiduk peatatakse, uksed avatakse
SulgeUksed() sulgeb uksed
Kiirenda(int kiirus) kirjutab üle baasklassi meetodi, kontrollib enne kiirendamist kas uksed on avatud.Kui uksed on avatud, siis ei saa kiirendada.
Hetkeseis() tagastab tekstina auto hetkeseisu (uksed avatud, seisab või sõidab kiirusega x)
ToString() panna väljastama Hetkeseis() poolt tagastatavat teksti
 */
