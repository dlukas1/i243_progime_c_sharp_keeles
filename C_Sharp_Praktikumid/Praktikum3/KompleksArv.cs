﻿using System;
namespace Praktikum3
/*
Loo klass Kompleksarv 
Realiseeri meetod liida, mis võimaldab liita ühele kompleksarvule teise
Realiseeri meetod lahuta, mis võimaldab ühest kompleksarvust lahutada teise
Z = A + Bi
*/
{
    public class KompleksArv
    {
        public int a { get; set; }
        public int b { get; set; }
        public string bi { get; set; } 

        public KompleksArv(int a, int b){
            this.a = a;
            this.b = b;

        }

        public KompleksArv(){}

        public KompleksArv liida(KompleksArv arv1, KompleksArv arv2){
            KompleksArv result = new KompleksArv();
            result.a = arv1.a + arv2.a;
            result.b = arv1.b + arv2.b;
            return result;
        }

        public string lahuta (KompleksArv a1, KompleksArv a2){
            return (a1.a - a2.a).ToString() + "" + (a1.b - a2.b).ToString() +"i";
        }
      
    }
}
