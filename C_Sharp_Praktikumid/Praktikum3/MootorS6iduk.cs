﻿using System;
namespace Praktikum3
/*
Luua klass Mootorsõidk
    Muutujad
-Kiirus(vaikimisi 0)
-MaxKiirus(vaikimisi 20)
    Konstruktor(int maxkiirus) – määratakse max kiirus
-Meetodid
-Kiirenda() lisab kiirusele 10
-Kiirenda(int kiirus) lisab kiirusele parameetri kiirus väärtuse ja kontrollib kas max kiirus on käes.Kui nii siis kiirus sellest suuremaks ei muutu.
-Stop() kiirus läheb nulli
-Hetkeseis() tagastab tekstina mootorsõiduki hetkeseisu (sõidab kiirusega x või seisab)
-ToString() panna väljastama Hetkeseis() poolt tagastatavat teksti
*/

{
    public class MootorS6iduk
    {
        private int kiirus = 0;
        private int maxKiirus = 20;

        public MootorS6iduk(int maxKiirus)
        {
            this.maxKiirus = maxKiirus;
        }
        public MootorS6iduk(){}

        public void Kiirenda() { kiirus = (kiirus + 10 <= maxKiirus) ? kiirus + 10 : maxKiirus; }

        public void Kiirenda(int x){
            kiirus = (kiirus + x <= maxKiirus) ? kiirus + x : maxKiirus;
        }

        public void Stop() { kiirus = 0; }

        public string HetkeSeis (){
            if (kiirus > 0)
            {
                return ("S6idab kiirusega " + kiirus);
            }
            else { return ("Seisab."); }
        }

        public override String ToString()
        {
            return HetkeSeis();

        }
    }
}
