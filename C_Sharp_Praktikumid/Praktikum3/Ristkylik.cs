﻿using System;

/*
Loo klass Ristkylik
Loo konstruktor, mis võtab sisendiks ristküliku pikkuse ja laiuse
Klassis olevad väljad on kõik privaatsed
Loo meetodid
    -arvutaPindala
    -arvutaYmbermoot
    -kasOnVordsed - kontrollib kas kaks ristkülikut on omavahel võrdsed
    -kasOnRuut
*/

namespace Praktikum3
{
    public class Ristkylik
    {
        private int Pikkus { get; set; }
        private int Laius { get; set; }

        public Ristkylik(int pikkus, int laius)
        {
            this.Pikkus = pikkus;
            this.Laius = laius;
        }
        //empty constructor
        public Ristkylik(){}

        public int arvutaPindala(Ristkylik r){
            return r.Pikkus*r.Laius;
        }

        public int arvutaYmbermoot(Ristkylik r){
            return (r.Pikkus+r.Laius) * 2;
        }

        public bool kasOnVordsed (Ristkylik r1, Ristkylik r2){
            if (r1.arvutaPindala(r1) == r2.arvutaPindala(r2)
                && r1.arvutaYmbermoot(r1) == r2.arvutaYmbermoot(r2))
            {
                return true;
            }
            else { return false; }
        }

        public bool kasOnRuut(Ristkylik r){
            if (r.Laius == r.Pikkus)
            {
                return true;
            }
            else { return false; }
        }
    }
}
