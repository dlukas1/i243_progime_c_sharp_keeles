﻿using System;
namespace Praktikum3
/*
Loo klass Punkt väljadega x ja y
Realiseeri järgnevad meetodid:
kaugusNullist - tagastatakse kaugus nullpunktist
teataAndmed - kirjutatakse välja punkti koordinaadi
kaugusTeisestPunktist - meetodile on võimalik kaasa anda teine punkt ning tagastatakse kaugus sellest punktist
kasOnAlgusPunkt - kontrollitakse, kas tegemist on koordinaatide alguspunktiga(0,0)
*/


{
    public class Punkt
    {
        
        public int x { get; set; }
        public int y { get; set; }

        public Punkt() {
            this.x = x;
            this.y = y;
        }

        public double kaugusNullist(){
            return Math.Round(Math.Sqrt(x * x + y * y)) ;
        }

        public void teataAndmed(){
            Console.WriteLine("Minu koordinaadid on x:{0} - y:{1}", x, y);
        }

        public double kaugusTeisestPunktist(Punkt p2){
            int rangeX = x - p2.x;
            int rangeY = y - p2.y;
            return Math.Sqrt(Math.Pow(rangeX,2) + Math.Pow(rangeY,2));
        }

        public bool kasOnAlgusPunkt(){
            return (x == 0 && y == 0) ? true : false;
        } 


       
    }
}
