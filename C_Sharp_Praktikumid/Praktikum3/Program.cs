﻿using System;

namespace Praktikum3
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            /*
            Ristkylik r = new Ristkylik();
			Ristkylik r1 = new Ristkylik(2, 3);
			Ristkylik r2 = new Ristkylik(3, 2);
            Ristkylik ruut = new Ristkylik(5, 5);

            if (r.kasOnRuut(ruut))
            { Console.WriteLine("Mina olen ruut!");}
            else
            {Console.WriteLine("Mina olen gladiolus!");}

            Console.WriteLine("r1 pindala on {0}", r.arvutaPindala(r1));
            Console.WriteLine("r2 ymbermoot on {0}", r.arvutaYmbermoot(r2));
            string compare = r.kasOnVordsed(r1, r2) ? " on vordsed " : " ei ole vordsed ";
            Console.WriteLine("Ristkylikud r1 ja r2 {0}", compare);

            Console.ReadLine();
            */
            /*
            Punkt p1 = new Punkt();
            p1.x = 3;
            p1.y = 3;
            Punkt p2 = new Punkt();
            p2.x = 0;
            p2.y = 0;

            Console.WriteLine("Minu kaugust nullist on {0}", p1.kaugusNullist());
            p1.teataAndmed();
            Console.WriteLine("Kaugus kahe punkti vahel on {0}", p1.kaugusTeisestPunktist(p2)); 

            if (p2.kasOnAlgusPunkt())
            {
                Console.WriteLine("Olen alguspunkt");
            } else{Console.WriteLine("Olen suvaline punkt"); }

            Console.ReadLine();
            */
            /*
            KompleksArv a1 = new KompleksArv(2, 3);
            KompleksArv a2 = new KompleksArv(1, 1);
            KompleksArv res = a1.liida(a1, a2);
            Console.WriteLine("Liitumise result on {0} + {1}i",res.a, res.b);
            string lahutatud = a1.lahuta(a2, a1);
            Console.WriteLine(lahutatud);
            */
            MootorS6iduk moto = new MootorS6iduk(30);
            moto.Kiirenda();
            Console.WriteLine(moto.ToString());//10
			moto.Kiirenda(15);
			Console.WriteLine(moto.ToString());//25
			moto.Kiirenda();
			Console.WriteLine(moto.ToString());//30
            moto.Stop();
            Console.WriteLine(moto.ToString());//Seisab
		}
    }
}
