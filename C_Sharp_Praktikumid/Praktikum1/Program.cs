﻿using System;

namespace Praktikum1
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			//GuessNumber();

			//CalculatePerimeter(1,2,3);

			//Console.WriteLine(GuessDate());

			//Console.WriteLine(GuessDate2());

			Console.WriteLine("Farengeiti scaala jargi see on " + CelsToFahr() + " graadi");




		}


		//Luua meetod, mis küsib kasutaja käest arvu ning teeb seda seni kaua,
		//kuni kasutaja on andnud korrektse sisendi

		public static void GuessNumber()
		{
			Console.Write("Guess number (from 1 to 10)");
			Random random = new Random();
			int rndNumber = random.Next(1, 11);
			while (true)
			{
				Console.Write("Your number: ");
				string input = Console.ReadLine();
				if (int.TryParse(input, out int userNumber))
				{
					if (rndNumber == userNumber)
					{
						Console.WriteLine("Congratulations, you won!");
						break;
					}
					else { Console.WriteLine("Close...try once more! "); }
				}
				//if tryParse == false ehk ei ole number
				else { Console.WriteLine("Invalid input!"); }
			}
		}


		//Luua meetod, mis arvutab kolmnnurga ümbermõõdu
		static void CalculatePerimeter(int a, int b, int c)
		{
			int perimeter = 0;
			if (a + b > c && a + c > b && b + c > a)
			{
				perimeter = a + b + c;
				Console.WriteLine("Perimeter is {0}", perimeter);
			}
			Console.WriteLine("It is not a triangle!");
		}


		//Luua meetod, mis küsib kasutajalt kuupäeva ning teeb seda seni kaua,
		//kuni kasutaja on sisestatnud korrektse kuupäeva

		static string GuessDate()
		{
			string result = "";

			Console.Write("What is the date today? (dd:mm:yyyy) ");
			string myDate = Console.ReadLine();
			string trueDate = DateTime.Today.ToString("d");
			if (myDate == trueDate)
			{ result = "Congratulations!"; }
			else
			{
				Console.WriteLine("Are you lost in time? Try once more!");
				GuessDate();
			}
			return result;
		}


		//Loo veel üks meetod, mis kirjutab kuupäeva ekraanile vastavalt 
		//etteantud kuupäevale ehk 
		static string GuessDate2()
		{
			string result = "";

			Console.Write("Mis kuupaev on tana? (dd.mm.yyyy) ");
			string myDate = Console.ReadLine();
			string trueDate = DateTime.Today.ToString("d");

			if (myDate == trueDate)
			{ result = "Tana!"; }
			else
			{
				string[] myDates = myDate.Split(".".ToCharArray());
				string[] trueDates = trueDate.Split(".".ToCharArray());

				if (myDates[2] == trueDates[2])
				{
					if (myDates[1] == trueDates[1])
					{
						if (Int32.Parse(myDates[0]) == (Int32.Parse(trueDates[0]) - 1))
						{
							result = "Eile";
						}
						else if (Int32.Parse(myDates[0]) == (Int32.Parse(trueDates[0]) + 1))
						{
							result = "homme";
						}
						else
						{
							result = (Int32.Parse(myDates[0]) < Int32.Parse(trueDates[0])) ?
							"enne" : "parast";
						}
					}
					//if year & month are correct
					else { result = trueDates[0] + "." + trueDates[1]; }
				}
				else
				{
					//if year is not correct
					result = trueDate;
				}
			}
			return result;
		}


		//Luua meetod, mis teisendab Celsisuse kraadid Fahrenheidiks
		static decimal CelsToFahr()
		{
			Console.Write("Sisesta celsiumi graadid: ");
			int cels = Int32.Parse(Console.ReadLine());

			//T(°F) = T(°C) × 9/5 + 32
			decimal fahr = (cels * 9 / 5) + 32;
			return fahr;
		}

	}
}
